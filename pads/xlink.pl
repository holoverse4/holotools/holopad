#!/usr/bin/perl
#
# This script take a pad.txt 
# and extract all the links in it
# in order to make a list of urn

my $file = shift;
my ($fpath,$bname,$ext) = &bname($file);
my @SULs = ();
my @WKL = ();
my %map = ();
local *F;
open F,'<',"$fpath/$bname.txt" or warn $!;
while (<F>) {
 chomp;
 if (m,((?:https?|mailto|callto|ip[fnm]s|ur[in])://[^ "'\)\]]*),) {
  my $url = $1;
  my $aid = &get_atomid($url);
  push @WKL,$1;
  printf "[K%s]: %s\n",$aid,$url;
   push @SUL, $1;
  push @SUL,'urn:web:'.$aid;
 }
 if (m,\[([^\]\[]+)\]\([^)]+\),) {
  my $info = $1;
  my $url = $2;
  printf "[%s]: %s\n",$1,$2;
  push @WKL,$1;
  push @SUL,$2;
  push @SUL,'urn:wiki:'.$1;
 }
  
}
close F;

exit $?;

sub get_atomid { # 17 char (to get all 20 Bytes)
  my $s = shift;
  my $nskey = &khash('SHA256',$s);
  my $hdr = pack'H*','1216';
  my $ns36 = &encode_base36($hdr.substr($nskey,0,20));
  my $aid = substr($ns36,0,17);
  return $aid;
}

sub khash { # keyed hash
   use Crypt::Digest qw();
   my $alg = shift;
   my $data = join'',@_;
   my $msg = Crypt::Digest->new($alg) or die $!;
      $msg->add($data);
   my $hash = $msg->digest();
   return $hash;
}


sub bname { # extract basename etc...
  my $f = shift;
  $f =~ s,\\,/,g; # *nix style !
  my $s = rindex($f,'/');
  my $fpath = ($s > 0) ? substr($f,0,$s) : '.';
  my $file = substr($f,$s+1);

  if (-d $f) {
    return ($fpath,$file);
  } else {
  my $p = rindex($file,'.');
  my $bname = ($p>0) ? substr($file,0,$p) : $file;
  my $ext = lc substr($file,$p+1);
     $ext =~ s/\~$//;

  $bname =~ s/\s+\(\d+\)$//;

  return ($fpath,$bname,$ext);

  }

}

sub encode_base36 {
  use Math::BigInt;
  use Math::Base36 qw();
  my $n = Math::BigInt->from_bytes(shift);
  my $k36 = Math::Base36::encode_base36($n,@_);
  #$k36 =~ y,0-9A-Z,A-Z0-9,;
  return $k36;
}

sub encode_basen { # n < 94;
  use Math::BigInt;
  my ($data,$radix) = @_;
  my $alphab = &alphab($radix);;
  my $mod = Math::BigInt->new($radix);
  #printf "mod: %s, lastc: %s\n",$mod,substr($alphab,$mod,1);
  my $h = '0x'.unpack('H*',$data);
  my $n = Math::BigInt->from_hex($h);
  my $e = '';
  while ($n->bcmp(0) == +1)  {
    my $c = Math::BigInt->new();
    ($n,$c) = $n->bdiv($mod);
    $e .= substr($alphab,$c->numify,1);
  }
  return scalar reverse $e;
}
# ---------------------------
sub alphab {
  my $radix = shift;
  my $alphab;
  if ($radix < 12) {
    $alphab = '0123456789-';
  } elsif ($radix == 12) {
    $alphab = '0123456789XE';
  } elsif ($radix <= 16) {
    $alphab = '0123456789ABCDEF';
  } elsif ($radix <= 26) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ';
  } elsif ($radix == 32) {
    $alphab = '0123456789ABCDEFGHiJKLMNoPQRSTUV'; # Triacontakaidecimal
    $alphab = join('',('A' .. 'Z', '2' .. '7')); # RFC 4648
    $alphab = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'; # Crockfordś ![ILOU] (U:accidental obscenity)
    $alphab = 'ybndrfg8ejkmcpqxotluwisza345h769';  # z-base32 ![0lv2]

  } elsif ($radix == 36) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789';
  } elsif ($radix <= 38) {
    $alphab = '0123456789ABCDEFGHiJKLMNoPQRSTUVWXYZ.-';
  } elsif ($radix <= 40) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789-_.+';
  } elsif ($radix <= 43) {
    $alphab = 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789 -+.$%*';
  } elsif ($radix == 58) {
    $alphab = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';
  } elsif ($radix == 62) {
    $alphab = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  } else { # n < 94
    $alphab = '-0123456789'. 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                             'abcdefghijklmnopqrstuwvxyz'.
             q/+.@$%_,~`'=;!^[]{}()#&/.      '<>:"/\\|?*'; #
  }
  # printf "// alphabet: %s (%uc)\n",$alphab,length($alphab);
  return $alphab;
}
1; # $Source: /my/perl/scripts/xlink.pl$
