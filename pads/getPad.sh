# 
set -xe
padid="$1"
period=annuel2
url="https://$period.framapad.org/p/$padid/export/markdown"
curl -s -L -o "_pad/$padid.md" "$url"
url="https://$period.framapad.org/p/$padid/export/txt"
curl -s -L -o "_pad/$padid.txt" "$url"
url="https://$period.framapad.org/p/$padid/export/etherpad"
curl -s -L -o "_pad/$padid.etherpath" "$url"
url="https://$period.framapad.org/p/$padid/export/pdf"
curl -s -L -o "_pad/$padid.pdf" "$url"
url="https://$period.framapad.org/p/$padid/export/html"
curl -s -L -o "_pad/$padid.html" "$url"
echo status: $?
cp -p _pad/$padid.html _pad/index.html

perl xlink.pl "_pad/$padid.txt" 

